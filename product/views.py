from django.shortcuts import render

# Create your views here.
from product.models import Product
from product.serializer import ProductSerializer
from rest_framework import viewsets, permissions


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
